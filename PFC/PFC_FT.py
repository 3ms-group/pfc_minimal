#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import math
from matplotlib.ticker import LinearLocator
import time
from itertools import cycle

def PFC2D_FT(u,u2,u3,kx2,ky2,ll,kk,dt):
    import numpy as np
    res=(u+dt*(kx2+ky2)*(-0.5*u2+0.3333*u3))/( 1.0-dt*(ll*(kx2+ky2)+kk*(2*(kx2+ky2)*(kx2+ky2)+(kx2+ky2)*(kx2+ky2)*(kx2+ky2))) ) 
    return res

plt.rc('text', usetex=True)
plt.rc('font', family='serif',size=14)

M=128*4
N =2*M                     
px = 4*np.pi/np.sqrt(3);
py = 2*np.pi;
Na=13*6
Lx=px*Na
Ly=px*Na #np.sqrt(3)*0.5*px
x = np.linspace(0,Lx,N,endpoint=True)
y = np.linspace(0,Ly,N,endpoint=True)
x=x-np.average(x)
y=y-np.average(y)
xm, ym = np.meshgrid(x, y)

kk = 0.459810;
ll = 0.6;
AA = 1-0.849;

I = complex(0,1)
kx = (2*np.pi/Lx)*np.array([I*y for y in list(range(0,M+1)) + list(range(-M+1,0)) ])
ky = (2*np.pi/Ly)*np.array([I*y for y in list(range(0,M+1)) + list(range(-M+1,0)) ])

kx2 = np.zeros((N,N), dtype=complex)
ky2 = np.zeros((N,N), dtype=complex)
for i in range(N):
    for j in range(N):
        kx2[i,j]=kx[i]**2
        ky2[i,j]=ky[j]**2

B=(2.-4.*AA+2.*np.sqrt(1.+16.*AA+20.*(kk-ll)-16.*AA*AA))/5.0
phi=0.5*(1-np.tanh(3*(np.sqrt(xm*xm+ym*ym)-3.0*px)/(3*px)))
uini = AA+B*(np.cos(2*np.pi*xm/px)*np.cos(2*np.pi*ym/(np.sqrt(3)*px))+0.5*np.cos(4*np.pi*ym/(np.sqrt(3)*px)))*phi


Nt=40000
t=0
dt=1.0
nplot=200
u=uini
uhat = np.fft.fft2(u)
EnergyAC=np.zeros(Nt)
tvec=np.zeros(Nt)
for i in range(Nt):
    print(i)
    u2=np.array(u**2, dtype=complex)
    u3=np.array(u**3, dtype=complex)
    u2_hat=np.fft.fft2(u2)
    u3_hat=np.fft.fft2(u3)
    uhat=PFC2D_FT(uhat,u2_hat,u3_hat,kx2,ky2,ll,kk,dt)
    u=np.real(np.fft.ifft2(uhat))
    tvec[i]=t
    if(i%nplot==0):
        plt.figure(1)
        plt.contourf(xm,ym,u,100)
        plt.title('Progress: '+"{:.2f}".format(t)+'/'+"{:.2f}".format(dt*Nt))
        plt.ylabel('$y$',size=14)
        plt.xlabel('$x$',size=14)
        plt.colorbar()
        plt.savefig("psi_"+str(i)+".png")
        #plt.show()
        plt.close(1)
        np.save("u"+str(i),u)
    t=t+dt;

