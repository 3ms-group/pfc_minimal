#!/usr/bin/env python
import numpy as np
import scipy as sci
import matplotlib.pyplot as plt
import math
import time
from IPython.display import display, clear_output
import os
from scipy.ndimage import gaussian_filter
from functions import *
#from numba import njit, prange

plt.rcParams.update({"text.usetex": True,"font.family": "Helvetica","font.size": 17})
plt.rc('text', usetex=True)
plt.rc('font', family='serif',size=17)

#@njit(parallel=True)
def SH2D_FT_lin(u, NL_u, kx2, ky2, dt, eps, alpha):
    k2 = kx2 + ky2  
    L_u = eps - alpha * (1.0 - 2 * k2 + k2**2) + 1E-10j
    expLu = np.exp(L_u * dt)
    term_u = (expLu - 1) / L_u
    res_u = expLu * u + term_u * NL_u
    return res_u

####
#Mesh and related quantities
####
N=258
px = 4*np.pi/np.sqrt(3); py = 2*np.pi;
Na=40;
Lx=py*(Na+1);Ly=py*(Na+1) 
x = np.linspace(-Lx/2,Lx/2,N,endpoint=True); y=x; xm, ym = np.meshgrid(x, y); dx=Lx/N
I = complex(0,1)
kx=2*np.pi*np.fft.fftfreq(N,Lx/N);ky=2*np.pi*np.fft.fftfreq(N,Ly/N)
kx1 = np.zeros((N,N), dtype=complex);ky1 = np.zeros((N,N), dtype=complex)
kx2 = np.zeros((N,N), dtype=complex);ky2 = np.zeros((N,N), dtype=complex)
for i in range(N):
    for j in range(N):
        kx1[i,j]=kx[i];ky1[i,j]=ky[j]
        kx2[i,j]=kx[i]**2;ky2[i,j]=ky[j]**2


####
# Initial Condition
####
idini=2
if(idini==0):
    #DEFECT DIPOLE (GLIDE)
    uux=np.zeros((N,N));uuy=np.zeros((N,N))
    uux,uuy=u_dipole(xm,ym,Lx,'g')
    theta_pert=0*np.pi/180
    fini=np.real(np.exp(1j*np.cos(theta_pert)*xm+1j*np.sin(theta_pert)*ym)*np.exp(1j*np.cos(theta_pert)*uux+1j*np.sin(theta_pert)*uuy))
if(idini==1):
    #CIRCULAR SEED
    r=Lx/10
    rad=np.sqrt(xm**2+ym**2)
    fini=np.real(np.exp(1j*xm))*(rad<r)
if(idini==2):
    #STRIPE
    r=Lx/5
    rad=xm**2
    fini=np.real(np.exp(1j*xm))*(abs(rad)<r)


# ADD NOISE
if(0):
     fini=fini+CCRand(xm,-0.5,+0.5)   
    
# Noise Dynamics
noise=0 #0 = no noise
noise_valmin=-noise
noise_valmax=noise


fig, axes = plt.subplots(figsize=(6, 6));
start_time = time.perf_counter()

#Parameters
eps=0.02
nu = 0.333
alpha=1
psibar=0.0
AA = (2./3.)*np.sqrt(3*eps-9*psibar**2)
val=1
EnergyAC=[]

#Time discretization
tmax=200
t=0
dt=0.1
Nt=int(tmax/dt+1)
nplot=100
tvec=np.zeros(Nt)

##
foldername="DIP_a"+str(alpha)+"_e"+str(eps)+"_ns"+str(noise)
os.makedirs(foldername,exist_ok=True)
##
save_parameters(["eps","nu","psibar","AA","alpha","noise","dt"], [eps,nu,psibar,AA,alpha,noise,dt], filename=foldername+"/parameters.txt")
##

#Initial Condition
uini = psibar+AA*fini#*(ym<0)
theta = np.ones((N,N))*0.0
u=uini
sq=5

for i in range(Nt):
    uhat = np.fft.fft2(u)
    #
    u3=np.array(u**3+CCRand(xm,noise_valmin,+noise_valmax), dtype=complex)
    u3_hat=np.fft.fft2(-u3)
    uhat=SH2D_FT_lin(uhat,u3_hat,kx2,ky2,dt,eps,alpha)
    u=np.real(np.fft.ifft2(uhat))
    tvec[i]=t
    if(i%nplot==0):
        
        print(str(i)+"/"+str(Nt))
        
        #Energy
        EnMap=energyFT(u,kx1,ky1,eps,alpha,dx)
        EnMapCoarse=gaussian_filter(EnMap, sigma=2*np.pi)
        EnergyAC=np.append(EnergyAC,np.sum(EnMap))
        np.savetxt(foldername+"/energy.txt", EnergyAC, fmt='%.2f')
        
        #Plot solution
        axes.contourf(xm,ym,u,50,cmap='Greys')
        axes.set_title('t: '+"{:.2f}".format(dt*i))
        axes.set_ylabel('$y$',size=14,rotation=0)
        axes.set_xlabel('$x$',size=14)
        
        #Saving & aux.
        if(i/nplot<10):
            aux0="000"
        elif(i/nplot<100):
            aux0="00"
        elif(i/nplot<1000):
            aux0="0"
        plt.savefig(foldername+"/psi_"+aux0+str(int(i/nplot))+".png",dpi=200)
        np.save(foldername+"/u"+str(i),u)
    t=t+dt;

end_time = time.perf_counter()

###
### GIF from png in foldername, saved in output_folder_gif
###
output_gif = foldername+"/DIP_a"+str(alpha)+"_e"+str(eps)+"_ns"+str(noise)+".gif"
allpng_to_GIF(foldername+"/.",output_gif,200)


print(f"Execution Time: {end_time - start_time:.6f} seconds")
