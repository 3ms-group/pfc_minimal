import numpy as np
import matplotlib.pyplot as plt
import random
from scipy.ndimage import gaussian_filter

def save_parameters(labels, values, filename="parameters.txt"):
    parameters = dict(zip(labels, values))
    with open(filename, "w") as file:
        for label, value in parameters.items():
            file.write(f"{label}: {value}\n")


def allpng_to_GIF(input_folder,output_gif,fm):
    from PIL import Image
    import os
    png_files = sorted([f for f in os.listdir(input_folder) if f.endswith(".png")])
    images = [Image.open(os.path.join(input_folder, file)) for file in png_files]
    images[0].save(
        output_gif,
        save_all=True,
        append_images=images[1:],
        duration=fm,  # Duration between frames in milliseconds
        loop=0         # Loop forever (use `loop=1` for one-time playback)
    )

def CCRand(x, vmin, vmax):
    random_noise = np.random.normal(size=x.shape)
    min_val = np.min(random_noise)
    max_val = np.max(random_noise)
    scale_factor = vmax - vmin
    if max_val - min_val > 1e-10:
        smooth_noise_normalized = (random_noise - min_val) / (max_val - min_val)
    else:
        smooth_noise_normalized = np.zeros_like(random_noise)
    scaled_noise = vmin + scale_factor * smooth_noise_normalized
    return scaled_noise

def plot_setting():
    plt.figure(figsize=(6, 6))
    #...

def laplacian_2d(f,dx):
    laplacian = np.zeros_like(f)
    for i in range(1,f.shape[0]-1):
        for j in range(1,f.shape[1]-1):
            laplacian[i,j] = (f[i+1,j] + f[i-1, j] + f[i,j+1] + f[i,j-1] - 4 * f[i, j])
    return np.array(laplacian)/(dx*dx)

def u_dislo_single(xmi,ymi,x0,y0,b):
    nu=1./3.
    ux=np.zeros(xmi.shape)
    uy=np.zeros(xmi.shape)
    xm=xmi-x0;ym=ymi-y0;
    ux=ux+(b/(2*np.pi))*(np.arctan2(ym,xm)+xm*ym/(2*(1-nu)*(xm**2+ym**2+0.001)))
    uy=uy-(b/(2*np.pi))*(((1-2*nu)/(4-4*nu))*np.log(xm**2+ym**2)+(xm**2-ym**2)/(4*(1-nu)*(xm**2+ym**2+0.001)))
    return ux,uy

def u_dipole(xmi,ymi,L,g):
    if(g=='g'):
        x1=-L/8
        x2=L/8
        y1=0
        y2=0
    elif(g=='c'):
        x1=0
        x2=0
        y1=-L/8
        y2=L/8
    uuxt,uuyt=u_dislo_single(xmi,ymi,x1,y2,(2*np.pi))
    uux,uuy=u_dislo_single(xmi,ymi,x2,y2,(-2*np.pi))
    uux=uux+uuxt
    uuy=uuy+uuyt
    return uux,uuy


def ddFF(u,kx,ky,ex,ey):
    I = complex(0,1)
    uhat=np.fft.fft2(u)
    derder=np.real(np.fft.ifft2( ( (I*ky)**ex ) * ( (I*kx)**ey )*uhat) )
    return derder

def ddFF2(u,kx,ky):
    I = complex(0,1)
    uhat=np.fft.fft2(u)
    derder=np.real(np.fft.ifft2( ( ( ((I*kx)**2) + ((I*ky)**2) )**2) * uhat ) )
    return derder

def energyFT(u,kx,ky,eps,alpha,dx):
    lap=ddFF(u,kx,ky,2,0)+ddFF(u,kx,ky,0,2)
    lap2=ddFF2(u,kx,ky)
    res1=-1.0*eps*(u**2)/2+alpha*((u**2)/2+0.25*(u**4)+u*(2*lap+lap2))#np.array(laplacian_2d(lap,dx))))
    return np.array(res1)
