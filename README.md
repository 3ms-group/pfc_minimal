# Minimal Phase Field codes implemented in Python

We provide here python scripts realizing minimal implementation of Phase Field models. The current repository is meant for didactic and illustrative purposes (and/or quick checks). The following models are included (list will be periodically updated)



### [Phase Field Crystal (PFC) model](https://gitlab.com/3ms-group/PF_minimal_codes/-/tree/main/PFC?ref_type=heads)

Fourier pseudo-spectral implementation (simple FE for time discretization). Minimal framework (e.g. outputs)

Refs: [Phys. Rev. Lett 88 245701 (2002)](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.88.245701); [Advances in Physics 61, 665 (2012)](https://www.tandfonline.com/doi/abs/10.1080/00018732.2012.737555).




### [Swift-Hohenberg (SH)](https://gitlab.com/3ms-group/PF_minimal_codes/-/tree/main/PFC?ref_type=heads)

Fourier pseudo-spectral implementation with exponential integrators. This example includes additional data output features (saving of parameters, creation of a GIF from single outputs, ...)

Refs: [Phys. Rev. A 15, 319 (1977)](https://journals.aps.org/pra/abstract/10.1103/PhysRevA.15.319); [Rev. Mod. Phys. 49, 435 (1977)](https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.49.435); 




## Usage

The scripts should work straightforwardly. Sample outputs are included.




## Acknoweldgements

For any usage beyond didactic purposes, please contact the developer(s) (3MS Group, marco.salvalaglio@tu-dresden.de).
